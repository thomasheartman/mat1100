% Created 2019-09-14 Sat 15:40
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\graphicspath{{./images/}}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Thomas Hartmann}
\date{\today}
\title{MAT1100: Obligatorisk innlevering 1}
\hypersetup{
 pdfauthor={Thomas Hartmann},
 pdftitle={Obligatorisk innlevering 1},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.1 (Org mode 9.2.3)},
 pdflang={English}}
\newcommand*\conj[1]{\bar{#1}}
\begin{document}

\maketitle
\tableofcontents

\section{Problem 1: Forms of complex numbers}
\label{sec:problem1}

\subsection{Write the complex number \(z = \frac{6}{\sqrt{3}+3i}\) in the form \(a + ib\)}
\label{sec:problem1pt1}

To convert \(z\) to polar form, we can split the number into a real and imaginary part to handle the respective fractions independently:
\begin{equation*}
  \frac{6}{\sqrt{3}+3i} = \frac{6}{\sqrt{3}} + \frac{6}{3i}
\end{equation*}

At this point, we can multiply the real part of the number by \(\sqrt{3}\), and simplify it:
\begin{equation*}
  \frac{6}{\sqrt{3}} = \frac{6\sqrt{3}}{3} = 2\sqrt{3}
\end{equation*}

As for the imaginary part, that simplifies to \(-2i\), leaving us with this for \(a + ib\):
\begin{equation*}
  2\sqrt{3}-2i
\end{equation*}

\subsection{Write the complex number \(z = \frac{6}{\sqrt{3}+3i}\) in the polar form \(re^{i\theta}\)}
\label{sec:problem1pt2}
To find the polar form of the number, we'll use the \(a + ib\) form. First, we find the modulus \(r\), which is done by calculating \(\sqrt{a^2 + b^2}\):
\begin{equation*}
  r = \sqrt{(2\sqrt{3})^2-2i^2} = \sqrt{12 + 4} = \sqrt{16} = 4
\end{equation*}

Next, we'll use the modulus to calculate the argument:
\begin{gather*}
  \cos\theta = \frac{a}{r}=\frac{2\sqrt{3}}{4} \\
  \sin\theta = \frac{b}{r}=\frac{-2}{4}=\frac{-1}{2} \\
\end{gather*}

Which we can then use to find the value of $\theta$:
\begin{gather*}
  \arccos\frac{2\sqrt{3}}{4} = \frac{\pi}{6} \\
  \arcsin\frac{-1}{2} = \frac{-\pi}{6}
\end{gather*}

Because we know that the argument has to be in the fourth quadrant (positive real part, negative imaginary part), we can conclude that $\theta = \frac{-\pi}{6}$, which means that the polar form of $z$ is $4e^{\frac{-\pi}{6}}$.

\section{Problem 2: Complex equations}
\label{sec:problem2}

\subsection{Find the two solutions to the equation \(w^2 - w + 1 = 0\)}
\label{sec:problem2pt1}

This is a simple quadratic equation with complex numbers, so we can just plug it into the quadratic formula and work from there:
\begin{equation*}
  x = \frac{1 \pm \sqrt{(-1)^2 - 4}}{2}
  =
 \frac{1 \pm \sqrt{1 - 4}}{2}
 =
 \frac{1 \pm \sqrt{-3}}{2}
 =
 \frac{1 \pm i\sqrt{3}}{2}
\end{equation*}

\subsection{Use what you found above to find all complex solutions for the equation \(z^4 - z^2 + 1 = 0\)}
\label{sec:problem2pt2}

Taking a step back and creating another abstraction for ourselves, we can see that if we assume $z = \sqrt{w}$, or rather $w = z^2$, then this second equation in $z$ is the same as the first equation in $w$. Based on this, the answer should still hold, but the solutions we found for $x$ in the first equation, are now the square of the solution for this second one. As such:
\begin{gather*}
  x^2 = \frac{1 \pm i\sqrt{3}}{2} \\
  x = \pm \sqrt{\frac{1 \pm i\sqrt{3}}{2}} = \pm\frac{\sqrt{3} \pm i}{2}
\end{gather*}

Which gives us all four solutions to the equation.

\subsection{Factor $z^4 - z^2 + 1 = 0$}
\label{sec:problem2pt3}

Because we know that $(z - r)(z - \conj{r}) = z^2 - 2az + (a^2 + b^2)$, we can substitute our values from the previous section's solution for $w$ for $a$ and $b$ here and end up with the following for the first equation:

\begin{gather*}
  r_w = \frac{1 \pm i\sqrt{3}}{2} \\
  (w - r)(w - \conj{r}) = 0
\end{gather*}

And because $w = z^2$ and the square roots of $r$ are the solutions to the second equation:
\begin{gather*}
  r_z = \pm\frac{\sqrt{3} \pm i}{2} \\
  (z - r_z)(z + r_z)(z - \conj{r_z})(z + \conj{r_z}) = 0
\end{gather*}

For the real second degree polynomial solution to the problem, we just need to rearrange the factors a bit:
\begin{gather*}
  (z - r_z)(z - \conj{r_z})(z + r_z)(z + \conj{r_z}) = 0 \\
  (z^2 - \sqrt{3}z + 1)(z^2 + \sqrt{3}z + 1) = 0 \\
\end{gather*}

\section{Problem 3: Limits}
\label{sec:problem3}

\subsection{$\lim_{n\to\infty} \frac{3n + 2}{\sqrt{4n^2 - 1}}$}
\label{sec:problem3pt1}

To tackle this limit, let's first move the square root outside of the fraction by squaring the numerator, and then expanding the resulting expression:
\begin{equation*}
  \lim_{n\to\infty} \frac{3n + 2}{\sqrt{4n^2 - 1}} = \lim_{n\to\infty} \sqrt{\frac{(3n + 2)^2}{4n^2 - 1}} = \lim_{n\to\infty} \sqrt{\frac{9n^2 + 12n + 4}{4n^2 - 1}}
\end{equation*}

From here, we can divide by the polynomial with the largest exponent, $n^2$:
\begin{equation*}
  \lim_{n\to\infty}\sqrt{\frac{9 + \frac{12}{n} + \frac{4}{n^2}}{4 - \frac{1}{n^2}}}
\end{equation*}

Most of the expressions tend to zero at this point, so as $n$ approaches $\infty$ we're left with the constants:
\begin{equation*}
  \sqrt{\frac{9}{4}} = \frac{3}{2}
\end{equation*}

Which means that:
\begin{equation*}
  \lim_{n\to\infty} \frac{3n + 2}{\sqrt{4n^2 - 1}} = \frac{3}{2}
\end{equation*}


\subsection{$\lim_{n\to\infty}(\sqrt{n^2 - 5n} - n)$}
\label{sec:problem3pt2}

For this problem, we're dealing with subtraction of two terms that tend towards $\infty$. Giving us a $\infty - \infty$ expression, which is quite useless. The first thing to do would be to transform the second term into an expression with a square root, allowing us to multiply by the conjugate in both the numerator and the denominator:
\begin{equation*}
  \lim_{n\to\infty}(\sqrt{n^2 - 5n} - n)
  =
  \lim_{n\to\infty}\frac{(\sqrt{n^2 - 5n} - n)(\sqrt{n^2 - 5n} + n)}{\sqrt{n^2 - 5n} + n}
\end{equation*}

Evaluating and simplifying this, we get the following:
\begin{equation*}
  \lim_{n\to\infty}\frac{n^2 - 5n - n^2}{\sqrt{n^2 - 5n} + n}
  =
  \lim_{n\to\infty}\frac{-5n}{\sqrt{n^2 - 5n} + n}
\end{equation*}

Now, let's divide by the common factor $n$ and work some magic on the resulting fraction:
\begin{equation*}
  \lim_{n\to\infty}\frac{-5}{\frac{\sqrt{n^2 - 5n}}{n} + 1}
  =
  \lim_{n\to\infty}\frac{-5}{\sqrt{\frac{n^2 - 5n}{n^2}} + 1}
  =
  \lim_{n\to\infty}\frac{-5}{\sqrt{\frac{n^2}{n^2} - \frac{5n}{n^2}} + 1}
\end{equation*}

Simplifying this even further, as $n$ approaches $\infty$, the second term under the square root approaches $0$:
\begin{equation*}
  \lim_{n\to\infty}\frac{-5}{\sqrt{1 - \frac{5n}{n^2}} + 1}
  =
  \lim_{n\to\infty}\frac{-5}{\sqrt{1} + 1}
  =
  \lim_{n\to\infty}\frac{-5}{1 + 1}
  =
  \lim_{n\to\infty}\frac{-5}{2}
\end{equation*}

So in conclusion:
\begin{equation*}
  \lim_{n\to\infty}(\sqrt{n^2 - 5n} - n) = -\frac{5}{2}
\end{equation*}


\section{Problem 4: Complex equations}
\label{sec:problem4}
Find the complex numbers $z$ that satisfy the equation $2|z - 1| = |z - 4|$.

By using a geometric intuition, we can think of this as all complex numbers $z$ where the distance to $1$ is exactly half of the distance to $4$. With the difference between $1$ and $4$ being $3$, that neatly divides into three parts, giving us $2$ as a very obvious candidate, with $-2$ showing up as a second option. However, we're interested in all complex numbers, so let's look a little closer.

\subsection{Solving the equation}
\label{sec:problem4pt1}

Using $|w| = \sqrt{a^2 + b^2}$ we can start to unpack the equation:
\begin{gather*}
  2|z - 1| = |z - 4| \\
  2\sqrt{(x - 1)^2 + y^2} = \sqrt{(x - 4)^2 + y^2} \\
  \sqrt{(2x - 2)^2 + 2y^2} = \sqrt{(x - 4)^2 + y^2} \\
  \sqrt{4x^2 - 8x + 4 + 4y^2} = \sqrt{x^2 - 8x + 16 + y^2} \\
  4x^2 - 8x + 4 + 4y^2 = x^2 - 8x + 16 + y^2 \\
\end{gather*}

Gathering $x$ and $y$ on the same side, we get:
\begin{gather*}
  3x^2 + 3y^2 = 12 \\
  x^2 + y^2 = 4 \\
\end{gather*}

This turns into a neat equation for a circle with a radius of $2$, as represented by this graph, where all the numbers that the line passes through, are values of $z$ that satisfy the equation:
\begin{figure*}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{graph}
  \caption{The complex numbers $z$ that satisfy the equation.}
  \label{fig:problem4.graph}
\end{figure*}

\section{Problem 5: Sequences and limits}
\label{sec:problem5}

The sequence $\{a_n\}$ is defined by
\begin{equation*}
  a_1 = 3
  \text{, }
  a_{n+1} = 3\sqrt{a_n}
  \text{ for }
  n \geq 1
\end{equation*}

\subsection{Show that $a_n < 9$ for all $n$}
\label{sec:problem5pt1}

\textbf{Proposition:} $P(n) = a_n < 9$ for all $n$.

Looking at the base case, we can easily see that
\begin{equation*}
  P(1) = 3 < 9
\end{equation*}

If we then go on to assume  that $P(n)$ is true, we can prove that $P(n+1)$ is also true:
\begin{gather*}
  P(n+1) = 3\sqrt{a_n} < 9 \\
\end{gather*}

Divide by $3$:
\begin{equation*}
  P(n+1) = \sqrt{a_n} < 3
\end{equation*}
Recognizing that $3 = \sqrt{9}$ we can simplify it:
\begin{equation*}
  P(n+1) = a_n < 9
\end{equation*}

Which we've already proved through our base case.


\subsection{Show that $a_{n+1} > a_n$ for all $n$}
\label{sec:problem5pt2}
\textbf{Proposition:} $a_{n+1} > a_n$ for all $n$.

Let's first look at the base case and assert that $P(1) = 3 < P(2) = 3\sqrt{3}$. Because $\sqrt{3}$ is larger than $1$, we know that this holds for the beginning of the sequence.

Now, for an arbitrary $n+1$, assuming that it holds for $P(n)$:
\begin{gather*}
  P(n+1) = 3\sqrt{a_n} > a_n \\
  P(n+1) = \sqrt{a_n} > {\frac{a_n}{3}} \\
\end{gather*}

Because $\sqrt{x}$ is larger than $\frac{x}{3}$ for all $x$ less than $9$ (that is, the square of the denominator), and because we know from the previous section that $a_n < 9$, this holds true.

\subsection{Convergence and $lim_{n\to\infty} a_n$}
\label{sec:problem5pt3}

We have proved in the previous sections that the sequence is monotonic and increasing, and that as $n$ approaches $\infty$, $a_n$ will always be less than $9$. By definition, a sequence converges towards a number $a$ iff for an arbitrary real number $\epsilon > 0$, there exists a number $N \in \mathbb{N}$ such that $|a_n - a| < \epsilon$  for all $n \geq N$. Based on this definition, we can conclude that the sequence is convergent.

As for the limit, if we assume that $\lim_{n\to\infty}a_n = a$, then we get the following:
\begin{equation*}
  lim_{n\to\infty}a_{n+1} = lim_{n\to\infty} 3\sqrt{a_n} = 3\sqrt{a}
\end{equation*}

We can solve this equation as follows:
\begin{gather*}
  a = 3\sqrt{a} \\
  \sqrt{a} = 3 \\
  a = 9
\end{gather*}

And that is our limit: $9$.

\end{document}
