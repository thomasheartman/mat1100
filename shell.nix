{ pkgs ? import <nixpkgs> {}}:
with pkgs;

let
in
stdenv.mkDerivation {
  name = "MAT1100";
  LANG="en_US.UTF-8";
  buildInputs = [
    texlive.combined.scheme-full
  ];
}
